package com.example.minigamealarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import javax.crypto.AEADBadTagException;

public class NewAlarmActivity extends AppCompatActivity {

    Button save_alarm;
    TimePicker timePicker;
    EditText alarm_name;
    ListView alarm_repeats;
    TextView repeat_day;

    int hour;
    int minute;
    String name = "";
    String challenge = "";

    ArrayList<String> days = new ArrayList<String>(
            Arrays.asList("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday")
    );

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_alarm);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        name = getIntent().getStringExtra("name");

        save_alarm = (Button) findViewById(R.id.btn_save);
        timePicker = (TimePicker) findViewById(R.id.timePicker);
        alarm_name = (EditText) findViewById(R.id.set_alarm_name);
        alarm_name.setText(name);

        repeat_day = findViewById(R.id.txt_repeats);
        repeat_day.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDialog();
            }
        });
        setAlarm();

    }

    private void  openDialog(){
        CheckboxDialog dialog = new CheckboxDialog("Repeat", days, this);
        dialog.show(getSupportFragmentManager(), "Repeat dialog");
    }

    private void setAlarm()
    {
        // UREDI ČAS
        hour = timePicker.getHour();
        minute = timePicker.getMinute();

        timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker timePicker, int i, int i1) {
                hour = timePicker.getHour();
                minute = timePicker.getMinute();
            }
        });

        alarm_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                name = editable.toString();
            }
        });

        save_alarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(name == null || name.isEmpty()){
                    name = String.format("Alarm %d", (DataRepository.getAlarmCount()+1 ));
                }

                Alarm alarm = new Alarm(Days.Ponedeljek, hour, minute, name, "Ponedeljek","");
                alarm.active = true;
                DataRepository.addAlarm(alarm);

                Calendar c = Calendar.getInstance();
                c.set(Calendar.HOUR_OF_DAY, hour);
                c.set(Calendar.MINUTE, minute);
                c.set(Calendar.SECOND, 0);

                setAlarmManager(c);

                Intent intent = new Intent(NewAlarmActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }

    private void setAlarmManager(Calendar c){
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, AlarmActivity.class);
        intent.setAction(Intent.ACTION_MAIN);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        alarmManager.setExact(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), pendingIntent);
    }

}
