package com.example.minigamealarm;

import java.util.Date;

public class Alarm {

    public Days repeats;
    public Time time;
    public boolean active;
    public String days;
    public String name;
    public String challenge;

    public Alarm(Days repeats, int hour, int minute, String name,String days, String challenge)
    {
        this.repeats = repeats;
        this.time = new Time(hour, minute);
        this.name = name;
        this.days = days;
        this.challenge = challenge;
    }

}

enum Days{
    Ponedeljek,
    Torek,
    Sreda,
    Četrtek,
    Petek,
    Sobota,
    Nedelja,
    Samo_enkrat,
    Vsak_dan
}