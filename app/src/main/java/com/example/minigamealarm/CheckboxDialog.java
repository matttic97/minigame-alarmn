package com.example.minigamealarm;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import java.util.ArrayList;
import java.util.Calendar;

public class CheckboxDialog extends AppCompatDialogFragment {

    private String Title;
    private ArrayList<String> Values;
    private Context Context;

    private ListView listView;

    public  CheckboxDialog(String title, ArrayList<String> values, Context context){
        Title = title;
        Values = values;
        Context = context;
    }

    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.listview_dialog, null);

        builder.setView(view)
                .setTitle(Title)
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                });

        listView = view.findViewById(R.id.set_checkboxListview);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            }
        });

        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        fillListview();

        return builder.create();
    }

    public interface CheckboxDialogListener{
        void applySelected(ArrayList<String> selected);
    }

    private void fillListview(){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                Context,
                R.layout.row_layout,
                R.id.txt_checkbox,
                Values
        );
        listView.setAdapter(adapter);
    }

}
