package com.example.minigamealarm;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class AlarmAdapter extends BaseAdapter {
    Activity context;
    ArrayList<Alarm> alarms;
    private static LayoutInflater inflater = null;

    public AlarmAdapter(Activity context, ArrayList<Alarm> alarms)
    {
        this.context = context;
        this.alarms = alarms;
        inflater = (LayoutInflater) context.getSystemService((Context.LAYOUT_INFLATER_SERVICE));
    }
    @Override
    public int getCount()
    {
        return alarms.size();
    }

    @Override
    public Alarm getItem(int position)
    {
      return alarms.get(position);
    }
    @Override
    public long getItemId(int position)
    {
        return position;
    }
    @Override
    public View getView(final int position, View covertedView, ViewGroup parent)
    {
        View itemView = covertedView;
        itemView = (itemView == null) ? inflater.inflate(R.layout.alarm_layout, null): itemView;
        TextView textViewName = (TextView) itemView.findViewById(R.id.alarm_name);
        TextView textViewDays = (TextView) itemView.findViewById(R.id.alarm_days);
        TextView textAlarmTime = (TextView) itemView.findViewById(R.id.alarm_time);
        Switch alarmOnOff = (Switch) itemView.findViewById(R.id.swt_on_off);
        Alarm selectedAlarm = alarms.get(position);
        textViewName.setText(selectedAlarm.name+",");
        textViewDays.setText(selectedAlarm.repeats.toString());
        textAlarmTime.setText(selectedAlarm.time.getTime());
        alarmOnOff.setChecked(selectedAlarm.active);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, NewAlarmActivity.class);
                intent.putExtra("name", alarms.get(position).name);
                context.startActivity(intent);
            }
        });
        return itemView;
    }
}
