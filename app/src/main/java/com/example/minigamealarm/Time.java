package com.example.minigamealarm;

public class Time {
    private int hour;
    private int minute;

    public Time(int hour, int minute){
        this.hour = hour;
        this.minute = minute;
    }

    public String getTime(){
        return this.hour + " : " + this.minute;
    }
}
