package com.example.minigamealarm;

import java.util.ArrayList;

public class DataRepository {
    private static ArrayList<Alarm> alarms = new ArrayList<Alarm>();

    public static void addAlarm(Alarm alarm){
        alarms.add(alarm);
    }

    public static ArrayList<Alarm> getAlarms(){
        return alarms;
    }

    public static int getAlarmCount(){
        return alarms.size();
    }

}
